#
#   Inheritance in Python
#

#The Parent class
class Dog:

    #Class Attribute
    species = 'mammal'

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def description(self):
         return"{} is {} years old".format(self.name, self.age)

    def speak(self, sound):
        return "{} says {}".format(self.name, sound)


#Child Class for inheritance

class Labradoodle(Dog):
    def run(self, speed):
        return "{} runs {}".format(self.name, speed)


#Child Class 2 for inheritance

class Bulldog(Dog):
    def run(self, speed):
        return "{} runs {}".format(self.name, speed)


#Overriding the Parent Class
class SomeBreed(Dog):
    species = 'lizard'

frank = Labradoodle("Frank", 3)
lizzer = SomeBreed("Lizard", 5)


#Because they have inherited from dog can call methods within, python does it via class inputs
Doggo = Bulldog("Doggo", 12)
print(Doggo.description())

#Plus its normal things inside
print(Doggo.run("Speedyboi"))

#Printing Override comparison
print(frank.species)
print(lizzer.species)
