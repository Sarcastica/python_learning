#
#   Storing objects in an object?!
#

from Dog_inherit import Dog, Bulldog, Labradoodle, SomeBreed

class Animal:

    dog_list = []

    def __init__(self, anType):
        self.anType = anType
        print ("This is a list of animal type " + str(self.anType))

    newDog = Bulldog('Tim', 3)
    steve = SomeBreed('Steve', 5)

    print(steve.species)

    print(newDog.name)

    dog_list.append(newDog)
    dog_list.append(steve)

    print("Printing an object from an object!")
    print(dog_list[0].name)

    print(". . .")
    print("Now to print all the list")

    dog_list.append(newDog)
    dog_list.append(newDog)
    dog_list.append(steve)
    dog_list.append(steve)

    for dogname in dog_list:
        print(dogname.name)
