#
#   Learning some Python with Dogs
#


class Dog:


    species = 'mammal'

    #Initialiser
    def __init__(self, name, age, breed):
        self.name = name
        self.age = age
        self.breed = breed

    #A Method for self
    def description(self):
        return"{} is {} years old!".format(self.name, self.age)

    #Talking Method
    def speak(self, sound):
        return"{} says {}".format(self.name, sound)

def oldest_dog(doglist):
    return max(doglist)


bono = Dog('Bono', 5, "German Shepard")
cosmo = Dog('Cosmo', 2, "Labrador")
bruce = Dog('Bruce', 8, " Shitzshu")

doglist = {bono.age, cosmo.age, bruce.age}

print("{} is {} and {} is {}.".format(bono.name, bono.age, cosmo.name, cosmo.age))

if cosmo.species == 'mammal':
    print("{0} is a {1}!".format(cosmo.name, cosmo.species))

print(cosmo.description())
print(cosmo.speak("Woof, Woof"))
print('The oldest dog is ' + str(oldest_dog(doglist)))
print(bruce.breed)
print(cosmo.breed)

