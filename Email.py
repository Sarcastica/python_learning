#
#   Modifying Attributes in Python Tutorial
#

class Email:

    def __init__(self):
        self.is_sent = False
        print(self.is_sent)

    def send_email(self):
        self.is_sent = True
        print(self.is_sent)

my_email = Email()
my_email.is_sent

my_email.send_email()
my_email.is_sent


